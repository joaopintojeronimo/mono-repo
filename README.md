# Why ?

Because a monorepo should not mean bad/unclear packaging, and a monorepo
should not mean a monolith.

# Goals

1) Easy to understand
  a) Packages are [node packages](https://docs.npmjs.com/how-npm-works/packages)
  b) Services are node packages that will run with the default "npm start", and have a dockerfile

2) Force standards but allow flexibility
  a) npm test will always be the way to run tests on a package, but you might need a special Dockerfile for your service
  b) Linting rules can be tweaked on a per-project basis

3) The top-level directory should just be used to stitch things together
  a) Provide an easy way to install and link everything for local development
  b) Make "yarn test" and "yarn lint" work

3) Easy local development
  a) All packages and services are linked when developing locally, so that changes in a package are instantly reflected in other places

4) Be as familiar as possible to anyone who has worked with node.js
  a) A package must be exactly as a node package, completely self contained;
  b) All commands like `npm test` and `npm start` must work as expected for all packages and services;
  c) Any package or service should be able to be published to a registry. We are probably never going to do it, but keeping that in mind gives us good restraints;

5) Promote good packaging
  a) What is logically a node package (i.e. a unit of functionality that most likely is re-used across other packages and services) should be self contained in a folder with a package.json and it's dependencies in node_modules like god intended;
  b) What is a service should be a valid node package as well, in the `services` folder, with a Dockerfile and all it's dependencies.

6) Atomic changes
  a) A change across packages and services should be atomic

7) Clear code standards
  a) Linting
  b) Clear separation of package unit tests, service unit tests and integration tests

8) Easy CI config
  a) docker-compose run integration-tests should test all the services together
  b) Integration tests should be easy to point at environments

# How this works

## 1. Clone the whole repo

    git clone git@github.com:travcorp/monorepo.git

## 2. Install

    cd monorepo
    yarn install

This will link all local dependencies (so that when we change a file in a dependency, the change is reflected on it's dependants) and install external dependencies for all packages and services

# TODO

* Stop linking everything once https://github.com/yarnpkg/yarn/issues/2165 is fixed
* Make a component library example with storybook..?
* Think about how to generate a collective code coverage report.
* Think about how we can have a better separation of build/tests and running in dockerfiles, so we don't ship dev dependencies and/or electron along with the application